
#beginning of function definition, three arguments are the vector of digit powers, and the test number in two forms.

fifth_power_sums<-function(powers,n_as_string, n_as_integer){
suitable<-0
    
    if(n_as_integer<1024){  
    result1<-sapply("4", function(x) length(grep(x,n_as_string)),USE.NAMES =FALSE)  #we don't want small numbers with 4s in
        if(result1==1){       
            suitable<-0
        }else {
           suitable<-1
        } 

    }else{
        suitable<-1
    }
                              
null_vector<-rep(0,5)
test_vector<-seq(5,9)
                       
    if(n_as_integer>10000) {     
    result2<-sapply(test_vector, function(x) length(grep(x,n_as_string)),USE.NAMES=FALSE)  #checks all of test vector against the number
  
        if(all.equal(result2,null_vector)==1){
            suitable<-0
        }else {
            suitable<-1
        }
    }
return(suitable)
}    #end of function definition and start of function call code.                      

power<- 5 #we could set this to 4 to check our code, with suitable modifications to the conditions.
                
powers<-vapply(seq(1,9),function(x) x^power,numeric(1))              
for (i in 12222:12235) {
    
    instance<-fifth_power_sums(powers,toString(i), i)#function call.  The variable 'instance' receives the value of 'suitable'

    if(instance==1){
      cat(i, "is a suitable number")
        #here we want to calculate the relevant sum for any suitable number
        #we can look up the entries of powers for this purpose
        }else{
     cat(i, "failed the tests. Move on!")#move straight to the next number in the loop
    }
}
