
library(psych)

data <- data.frame(ID=seq(1,25,1), Age=sample(18:99,25,replace=TRUE),
                   Gender=sample(1:2,25,replace=TRUE),
                   Score=sample(0:50,25,replace=TRUE))
ageSum <- describe(data$Age)
ageSum

ageGenSum <- describeBy(data$Age,data$Gender)
ageGenSum
